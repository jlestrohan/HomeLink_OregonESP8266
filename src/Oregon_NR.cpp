﻿#include "Oregon_NR.h"

//Tout ce qui concerne l'interruption/////////////////////////////////////
#ifndef Oregon_NR_int
#define Oregon_NR_int
static volatile unsigned long pm;
static volatile unsigned long pl, timer_mark;

static int RECEIVER_PIN;

// ICACHE_RAM_ATTR to avoid crashes due to interrupt hog
void IRAM_ATTR receiver_interruption(void)
{
	if (digitalRead(RECEIVER_PIN))
	{
		//Début de l'impulsion
		pl = 0;
		pm = micros();
	}
	else
	{
		//Fin du pouls
		//Calcule l'heure de fin et la longueur
		pl = micros() - pm;
		pm += pl;
	}
	//yield();
}
#endif

//////////////////////////////////////////////////////////////////////
Oregon_NR::Oregon_NR(byte MHZ, bool pull_up)
{
	INT_NO = digitalPinToInterrupt(MHZ);
	PULL_UP = pull_up;
	RECEIVER_PIN = MHZ;
	pinMode(MHZ, INPUT); // 433 receiver pin
}

Oregon_NR::Oregon_NR(byte MHZ, byte led, bool pull_up)
{
	INT_NO = digitalPinToInterrupt(MHZ);
	LED = led;
	PULL_UP = pull_up;
	RECEIVER_PIN = MHZ;
	pinMode(MHZ, INPUT);  // 433 receiver pin
	pinMode(LED, OUTPUT); // LED output pin
}

//////////////////////////////////////////////////////////////////////
void Oregon_NR::start()
{
	packet_number = 0;
	packets_received = 0;
	start_pulse_cnt = 0;
	receive_status = FIND_PACKET;
	led_light(false);
	attachInterrupt(INT_NO, receiver_interruption, CHANGE);
}
//////////////////////////////////////////////////////////////////////
void Oregon_NR::stop()
{
	detachInterrupt(INT_NO);
}
//////////////////////////////////////////////////////////////////////
//Capture et analyse de package
//DEBUG_INFO - la capture de données est affichée en série
//////////////////////////////////////////////////////////////////////
void Oregon_NR::capture(bool DEBUG_INFO)
{
	// Back to initial state
	maybe_packet = 0;
	packets_received = 0;
	sens_type = 0;
	crc_c = 0;
	captured = 0;
	data_val = 0;
	data_val2 = 0;

	// Reading receiver data
	noInterrupts();
	pulse_length = pl;
	pl = 0;
	pulse_marker = pm;
	interrupts();

	// Pulse detected
	if (pulse_length != 0 && receive_status == FIND_PACKET)
	{
		if (start_pulse_cnt == 0)
		{
			// found the first seemingly correct pulse
			if (pulse_length < (PER_LENGTH + 16) && pulse_length > (THR_LENGTH))
			{
				start_pulse_cnt = 1;
				pre_marker[start_pulse_cnt] = pulse_marker;
				pulse_length = 0;
			}
		}
		else
		{
			// Found the next good pulse
			if (pulse_length <= (PER_LENGTH + 16) && pulse_length >= (THR_LENGTH))
			{
				// If that pulse is alocated at the right place, add in the starting pulse counter
				if (pulse_marker - pre_marker[start_pulse_cnt] > (PER_LENGTH * 2 - LENGTH_TOLERANCE) && pulse_marker - pre_marker[start_pulse_cnt] < (PER_LENGTH * 2 + LENGTH_TOLERANCE))
				{
					start_pulse_cnt++;
					pre_marker[start_pulse_cnt] = pulse_marker;
					pulse_length = 0;
				}
				//La prochaine impulsion est au mauvais endroit.
				//Attribuez-le d'abord
				else
				{
					start_pulse_cnt = 1;
					pre_marker[start_pulse_cnt] = pulse_marker;
					pulse_length = 0;
				}
			}
			else
			{
				///////////////////////////////////////////////////////////
				//Si la durée de l'impulsion est incorrecte, il convient de vérifier,
				//Mais n'attendons pas l'impulsion correcte
				if (pulse_marker - pre_marker[start_pulse_cnt] < (PER_LENGTH * 2 + LENGTH_TOLERANCE))
				{
					///////////////////////////////////////////////////////////
					//Le moment n’est pas encore venu, c’est très probablement un obstacle.Sauter..
					pulse_length = 0;
				}
				else
				{
					///////////////////////////////////////////////////////////
					//Le temps est écoulé, on recommence à chercher
					start_pulse_cnt = 0;
					pulse_length = 0;
				}
			}
		}
	}
	// As soon as a full packet is detected we start procesing it,
	// even if there's a second packet awaiting its turn, too bad for it
	// let's just start analysing what we have for now
	if (packet_number == 1 && (millis() - first_packet_end) > 200)
		receive_status = ANALYZE_PACKETS;
	if (packet_number == 1 && !is_assemble)
		receive_status = ANALYZE_PACKETS;

	// Start collecting data
	if (start_pulse_cnt == 3 && receive_status == FIND_PACKET)
	{

		work_time = millis();
		last_premarker = pre_marker[3];
		start_pulse_cnt = 0;
		if (packet_number == 0)
		{
			read_tacts = collect(collect_data);
			first_packet_end = millis();
			packet_number = 1;
		}
		else
		{
			read_tacts2 = collect(collect_data2);
			packet_number = 2;
			receive_status = ANALYZE_PACKETS;
		}
	}

	// Data analysis
	if (receive_status == ANALYZE_PACKETS)
	{
		//Interruption off to speed up the process
		detachInterrupt(INT_NO);

		led_light(true);

		//Collected data dump
		if (DEBUG_INFO)
		{
			for (int bt = 0; bt < 180; bt++)
			{
				Serial.print(collect_data[bt], HEX);
				Serial.print(' ');
			}
			Serial.println(" ");

			for (int bt = 0; bt < 180; bt++)
			{
				Serial.print(collect_data2[bt], HEX);
				Serial.print(' ');
			}
			Serial.println(" ");
		}

		//////////////////////////////////////////////
		//Обработка первой записи
		//Расшифровываем запись. Данные сохраянем в decode_tacts[]
		get_bits(collect_data);
		bool halfshift;
		if (get_data(0, collect_data) > get_data(1, collect_data))
		{
			data_val = get_data(0, collect_data);
			halfshift = 0;
		}
		else
		{
			data_val = get_data(1, collect_data);
			halfshift = 1;
		}
		//////////////////////////////////////////////
		//Ищем позицию синхронибла
		synchro_pos = get_synchro_pos(collect_data);
		//////////////////////////////////////////////
		//Выводим посылку
		if (DEBUG_INFO)
		{
			Serial.print("1)     ");
			for (int bt = 0; bt < READ_BITS; bt++)
			{
				if (bt <= read_tacts / 2)
				{
					if (collect_data[bt] > 128 + 1)
						Serial.print('I');
					if (collect_data[bt] < 128 - 1)
						Serial.print('O');
					if (collect_data[bt] == 128 + 1)
						Serial.print('i');
					if (collect_data[bt] == 128 - 1)
						Serial.print('o');
					if (collect_data[bt] == 128)
						Serial.print('.');
				}
				else
					Serial.print(' ');
			}
			Serial.print(" SIZE:");
			Serial.print(read_tacts);
			Serial.print(" VAL:");
			Serial.print(data_val);
			Serial.print(" SYN:");
			Serial.print(synchro_pos);
			Serial.print(" SHIFT:");
			Serial.println(halfshift);
		}
		//////////////////////////////////////////////
		//Аналогично обрабатываем вторую запись
		if (packet_number == 2)
		{
			get_bits(collect_data2);
			if (get_data(0, collect_data2) > get_data(1, collect_data2))
			{
				data_val2 = get_data(0, collect_data2);
				halfshift = 0;
			}
			else
			{
				data_val2 = get_data(1, collect_data2);
				halfshift = 1;
			}
			synchro_pos2 = get_synchro_pos(collect_data2);
			if (DEBUG_INFO)
			{
				Serial.print("2)     ");
				for (int bt = 0; bt < READ_BITS; bt++)
				{
					if (bt <= read_tacts2 / 2)
					{
						if (collect_data2[bt] > 128 + 1)
							Serial.print('I');
						if (collect_data2[bt] < 128 - 1)
							Serial.print('O');
						if (collect_data2[bt] == 128 + 1)
							Serial.print('i');
						if (collect_data2[bt] == 128 - 1)
							Serial.print('o');
						if (collect_data2[bt] == 128)
							Serial.print('.');
					}
					else
						Serial.print(' ');
				}
				Serial.print(" SIZE:");
				Serial.print(read_tacts2);
				Serial.print(" VAL:");
				Serial.print(data_val2);
				Serial.print(" SYN:");
				Serial.print(synchro_pos2);
				Serial.print(" SHIFT:");
				Serial.print(halfshift);
			}
		}
		byte *result_data; //, result_data_start, aux_data;
		int correlation;

		//////////////////////////////////////////////
		//СОПОСТАВЛЕНИЕ ПАКЕТОВ
		//Если пакет один, то и сопоставлять не из чего
		if (packet_number == 1)
			result_data = collect_data;
		//////////////////////////////////////////////
		//А вот если два, то нужна СБОРКА ПАКЕТА
		//вычисляем оптимальное "смещение" пакетов друг относительно друга
		if (packet_number == 2)
		{
			correlation = correlate_data(collect_data, collect_data2);
			if (DEBUG_INFO)
			{
				Serial.print(" COR: ");
				Serial.println(correlation);
			}
			//////////////////////////////////////////////
			//Собираем данные в пакет, где синхронибл найден раньше
			//////////////////////////////////////////////
			if (synchro_pos >= synchro_pos2)
			{
				result_size = read_tacts2;
				result_data = collect_data2;
				correlation = -correlation;
				assemble_data(collect_data2, collect_data, correlation);
			}
			else
			{
				result_size = read_tacts;
				result_data = collect_data;
				assemble_data(collect_data, collect_data2, correlation);
			}
		}
		//////////////////////////////////////////////
		//Вывод готовой посылки
		if (DEBUG_INFO && packet_number == 2)
		{
			Serial.print("RESULT ");
			byte *rdt = result_data;
			for (int bt = 0; bt < READ_BITS; bt++)
			{
				if (bt <= result_size / 2)
				{
					if (*rdt > 128 + 1)
						Serial.print('I');
					if (*rdt < 128 - 1)
						Serial.print('O');
					if (*rdt == 128 + 1)
						Serial.print('i');
					if (*rdt == 128 - 1)
						Serial.print('o');
					if (*rdt == 128)
						Serial.print('.');
				}
				else
					Serial.print(' ');
				rdt++;
			}
			Serial.println(" ");
		}
		// extracting bits from the clock
		sens_type = 0;
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
		if (get_info_data(result_data, packet, valid_p))
#pragma GCC diagnostic pop
		{
			sens_type = get_sensor(packet);		  //We determine the type of package by type of sensor
			restore_data(packet, sens_type);	  // Restore data by sensor type
			crc_c = check_CRC(packet, sens_type); // Check CRC, if it is true, then make all doubtful bits sure
			//If not all bytes are determined reliably, it cannot be assumed that the packet is correct.
			byte secresingV;
			if (sens_type == THGN132 || (sens_type & 0xFF00) == GAS)
				secresingV = PACKET_LENGTH - 4;
			if (sens_type == THN132)
				secresingV = PACKET_LENGTH - 6;
			for (byte www = 0; www < (PACKET_LENGTH - secresingV + 2); www++)
				if (valid_p[www] < 0x0f)
					crc_c = false;
			//The packet is captured only if the starting sequence (synchronization nibl) is found
			captured = 1;
		}
		else if (data_val >= 64 || data_val2 >= 64)
			maybe_packet = 1;
		//Transcript Oregon Sensors
		if ((sens_type == THGN132 || sens_type == THN132) && crc_c)
		{
			sens_tmp2 = 404;
			sens_CO = 255;
			sens_CH = 255;
			sens_id = get_id(packet);
			sens_chnl = get_channel(packet);
			sens_battery = get_battery(packet);
			sens_tmp = get_temperature(packet);
			if (sens_type == THGN132)
				sens_hmdty = get_humidity(packet);
		}

		//Decryption of integrated gas sensors
		if ((sens_type & 0xFF00) == GAS && crc_c)
		{
			sens_id = 0;
			sens_battery = 0;
			sens_chnl = get_gas_channel(packet);
			sens_tmp = get_gas_temperature_out(packet);
			if (packet[9] == 0x0F)
				sens_tmp = 404;
			sens_tmp2 = get_gas_temperature_in(packet);
			if (packet[12] == 0x0F)
				sens_tmp2 = 404;
			sens_hmdty = get_gas_hmdty(packet);
			sens_CO = get_gas_CO(packet);
			sens_CH = get_gas_CH(packet);
		}

		// fire alarm sensors
		if ((sens_type & 0xFF00) == FIRE && crc_c)
		{
			sens_id = 0;
			sens_battery = 0;

			sens_chnl = get_gas_channel(packet);
			sens_ip22 = get_fire_ip22(packet);
			sens_ip72 = get_fire_ip72(packet);
			sens_lockalarm = get_fire_lockalarm(packet);
		}

		//Other calculations
		//We return everything to its original state and turn on listening to the receiver
		work_time = millis() - work_time;
		packets_received = 0;
		if (data_val >= 64 && synchro_pos != 255)
			packets_received++;
		if (data_val2 >= 64 && synchro_pos2 != 255)
			packets_received++;
		receive_status = FIND_PACKET;
		start_pulse_cnt = 0;
		packet_number = 0;
		led_light(false);
		//Serial.print("LED = ");
		//Serial.println(LED);
		attachInterrupt(INT_NO, receiver_interruption, CHANGE);
	}
}

//Extracts from the clock sequence - bit
//Parameters: cdptr - pointer to a recorded clock sequence
void Oregon_NR::get_bits(byte *cdptr)
{

	//Сброс массивов
	byte *cdp = cdptr;
	for (int bt = 0; bt < READ_BITS * 2; bt++)
		decode_tacts[bt] = 2;

	for (int bt = 0; bt < READ_BITS * 2; bt++)
	{

		if ((*cdp & 0xf0) > 0x20 && (*cdp & 0x0f) > 0x04)
			decode_tacts[bt] = 1;
		if ((*cdp & 0xf0) < 0x30 && (*cdp & 0x0f) < 0x05)
			decode_tacts[bt] = 0;
		if ((*cdp & 0xf0) < 0x20 && (*cdp & 0x0f) > 0x04)
			decode_tacts[bt] = 4;
		if ((*cdp & 0xf0) > 0x40 && (*cdp & 0x0f) < 0x02)
			decode_tacts[bt] = 3;
#pragma GCC diagnostic ignored "-Wunused-value"
		*cdp++;
#pragma GCC diagnostic pop
	}
	return;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Extrait une séquence d'horloge d'un enregistrement de canal
// Paramètres: cdptr - pointeur sur les données enregistrées
// btt - offset en cycles. Le décalage par cycle dans l'analyse peut aider à restaurer le package, ce qui a détruit le début
// La fonction renvoie la qualité ou la "validité" du décodage - le nombre de ticks reconnus avec confiance.
// En comparant la condition physique avec btt = 0 et btt = 1, choisissez le meilleur
////////////////////////////////////////////////////////////////////////////////////////////////////
int Oregon_NR::get_data(int btt, byte *cdptr)
{ //btt - смещение на такт при анализе может поммочь восстановить пакет, у которого разрушено начало

	byte *cdp = cdptr;
	for (int bt = 0; bt < READ_BITS; bt++)
	{

		*cdp = 128;
		cdp++;
	}
	cdp = cdptr;
	*cdp = (128 + 2);
	cdp++;
	int packet_validity = 0;
	for (int bt = 1; bt < READ_BITS; bt++)
	{

		if (decode_tacts[bt * 2 - btt] == 0)
			*cdp -= 1;
		if (decode_tacts[bt * 2 - btt] == 1)
			*cdp += 1;
		if (decode_tacts[bt * 2 - 2 - btt] == 1 && decode_tacts[bt * 2 - 1 - btt] == 4)
			*cdp -= 1;
		if (decode_tacts[bt * 2 - 2 - btt] == 0 && decode_tacts[bt * 2 - 1 - btt] == 3)
			*cdp += 1;
		if (decode_tacts[bt * 2 - 2 - btt] == 0 && decode_tacts[bt * 2 - 1 - btt] == 1)
			*cdp -= 1;
		if (decode_tacts[bt * 2 - 2 - btt] == 1 && decode_tacts[bt * 2 - 1 - btt] == 0)
			*cdp += 1;

		if (decode_tacts[bt * 2 + 2 - btt] == 1 && decode_tacts[bt * 2 + 1 - btt] == 3)
			*cdp -= 1;
		if (decode_tacts[bt * 2 + 2 - btt] == 0 && decode_tacts[bt * 2 + 1 - btt] == 4)
			*cdp += 1;
		if (decode_tacts[bt * 2 + 2 - btt] == 0 && decode_tacts[bt * 2 + 1 - btt] == 1)
			*cdp -= 1;
		if (decode_tacts[bt * 2 + 2 - btt] == 1 && decode_tacts[bt * 2 + 1 - btt] == 0)
			*cdp += 1;

		//Подсчитываем кол-во достоверных бит в пакете
		if (*cdp > (128 + 1))
			packet_validity += *cdp - 128;
		if (*cdp < (128 - 1))
			packet_validity += 128 - *cdp;
		cdp++;
	}
	return packet_validity;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Écoute d'une chaîne avec une fréquence d'échantillonnage de 16384 Hz
// cdptr - pointeur sur la zone de mémoire où le signal est écrit
// dtl - un pointeur sur le nombre de cycles de lecture
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::collect(byte *cdptr)
{

	//bool cdp_prev_null;
	byte *cdp = cdptr;
	byte nulls_found = 0;
	//////////////////////////////////////////////////////
	//Nous commençons l’enregistrement à partir de ce point (la fin de la dernière impulsion du cycle principal + 1/16)
	unsigned long tmp_marker = last_premarker + PER_LENGTH / 32;
	byte bt2 = 0;
	//////////////////////////////////////////////////////
	//	Les deux premières barres sont des unités.Nous avons attrapé l'impulsion!
	*cdp = 0x88;
	cdp++;
	while (micros() <= tmp_marker)
		;
	//////////////////////////////////////////////////////
	//Nous commençons à lire les données en mémoire
	// Lire le logiciel maximum 90 bits
	// POSITION DE THN - 96BIT, THN - 76 bits + au moins 3 bits 111 que nous avons déjà trouvés
	byte bt;
	for (bt = 0; bt < READ_BITS2; bt++)
	{
		*cdp = 0;
		for (byte ckl = 0; ckl < 8; ckl++)
		{ // 	Lire 8 fois pour un demi - tour
			if (digitalRead(RECEIVER_PIN))
				*cdp += 0x10; // Les mesures sont écrites dans le quartet supérieur.
			tmp_marker += PER_LENGTH / 16;
			while (micros() < tmp_marker)
				;
		}
		last_premarker += PER_LENGTH / 2;
		tmp_marker = last_premarker + PER_LENGTH / 32;
		for (byte ckl = 0; ckl < 8; ckl++)
		{
			if (digitalRead(RECEIVER_PIN))
				*cdp += 1; // La demi - mesure suivante de la mesure est enregistrée dans le quartet inférieur.Ça économise de la mémoire.
			tmp_marker += PER_LENGTH / 16;
			while (micros() < tmp_marker)
				;
		}
		last_premarker += PER_LENGTH / 2;
		bt2++;
		//  Tous les 8 cycles, ajoutez 5 µs pour égaliser la période de 976 µs à 976,56 µs
		if (bt2 == 7)
		{
			last_premarker += 4;
			bt2 = 0;
		}
		tmp_marker = last_premarker + PER_LENGTH / 32;
		cdp++;
		/////////////////////////////////////////////
		//	Il y a un temps avant l'arrivée de la prochaine moitié
		// Vous pouvez vérifier si le colis est terminé
		// Si le canal est récemment vide, cela devrait être noté.

		//if ((*cdp&0xf0)<0x30 && (*cdp&0x0f)<0x05) decode_tacts[bt]=0;
		//if (*cdp == 0 && (*(cdp - 1) == 0)) nulls_found++;
		yield();
		if ((*cdp & 0xf0) < 0x30 && (*cdp & 0x0f) < 0x05 && ((*(cdp - 1) & 0xf0) < 0x30 && (*(cdp - 1) & 0x0f) < 0x05))
			nulls_found++;
		else
			nulls_found = 0;
		/////////////////////////////////////////////
		//Si plusieurs champs empty_space vides dans l'enregistrement, alors
		// c'est probablement la fin du paquet. Cela n'a aucun sens de lire plus loin
		// empty_space - nombre empirique, dépend du type de récepteur et du niveau du signal
		// Si réduit, il est possible de confondre avec des dommages de paquet.
		// Si vous augmentez, vous ne pouvez pas arrêter de lire et commencer à enregistrer du bruit
		if (nulls_found > empty_space)
			return bt;
		/////////////////////////////////////////////
		//Nous attendons l'arrivée de la prochaine mi-temps
		while (micros() < last_premarker)
			;
	}
	return bt;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Détermination du décalage de paquet l'un par rapport à l'autre
// En tant que paramètres, les pointeurs vers les tableaux de données sont passés
// Retour offset
//> 0 - le second package a démarré plus tôt, <0 - le premier package a démarré plus tôt
////////////////////////////////////////////////////////////////////////////////////////////////////
int Oregon_NR::correlate_data(byte *ser1, byte *ser2)
{

	byte best_correl = 0;
	int best_shift = 0;
	byte best_correl_back = 0;
	int best_shift_back = 0;
	byte shift_score[READ_BITS];
	byte *s1;
	byte *s2;
	byte *s2t = ser2;
	//on décale le premier paquet par rapport au deuxième
	for (byte sht = 0; sht < READ_BITS; sht++)
	{
		s1 = ser1;
		s2 = s2t;
		shift_score[sht] = 0;
		for (byte sp = 0; sp < READ_BITS - sht; sp++)
		{
			if ((*s1 > (128 + 1) && *s2 > (128 + 1)) || (*s1 < (128 - 1) && *s2 < (128 - 1)))
				shift_score[sht]++;
			s2++;
			s1++;
		}
		yield();
		s2t++;
	}
	for (int i = 0; i < READ_BITS; i++)
	{

		if (shift_score[i] > best_correl)
		{
			best_correl = shift_score[i];
			best_shift = i;
		}
	}

	//Maintenant, le contraire est le premier paquet par rapport au premier

	byte *s1t = ser1;
	for (byte sht = 0; sht < READ_BITS; sht++)
	{
		s2 = ser2;
		s1 = s1t;
		shift_score[sht] = 0;
		for (byte sp = 0; sp < READ_BITS - sht; sp++)
		{

			if ((*s1 > (128 + 1) && *s2 > (128 + 1)) || (*s1 < (128 - 1) && *s2 < (128 - 1)))
				shift_score[sht]++;
			s2++;
			s1++;
		}
		yield();
		s1t++;
	}
	// Nous recherchons le meilleur match pour les deux options.

	for (int i = 0; i < READ_BITS; i++)
	{

		if (shift_score[i] > best_correl_back)
		{
			best_correl_back = shift_score[i];
			best_shift_back = i;
		}
	}
	//Et retourne le meilleur des deux
	if (best_correl_back > best_correl)
		return -best_shift_back;
	else
		return best_shift;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Construction de deux paquets
// En tant que paramètres, les pointeurs vers les tableaux de données sont passés
// Et le paquet de résultats devrait aller en premier, c'est-à-dire celui qui a un préambule plus long.
// shift - offset du second paquet par rapport au premier
////////////////////////////////////////////////////////////////////////////////////////////////////
void Oregon_NR::assemble_data(byte *s1, byte *s2, int shift)
{

	if (shift >= 0)
	{
		for (int g = 0; g < shift; g++)
			s2++;
		for (int i = 0; i < READ_BITS - shift; i++)
		{
			if (*s1 < (128 + 2) && *s1 > (128 - 2) && (*s2 > (128 + 1) || *s2 < (128 - 1)))
			{
				*s1 = *s2;
			}
			s1++;
			s2++;
		}
	}
	else
	{
		for (int g = 0; g < -shift; g++)
			s1++;
		for (int i = 0; i < READ_BITS + shift; i++)
		{
			if (*s1 < (128 + 2) && *s1 > (128 - 2) && (*s2 > (128 + 1) || *s2 < (128 - 1)))
			{
				*s1 = *s2;
			}
			s1++;
			s2++;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Renvoie la position synchrone dans le package.
// 0xFF - pas de synchronisation
// code - un pointeur sur la séquence de bits déchiffrée
// résultat - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_synchro_pos(byte *code)
{

	bool syn_found = false;
	byte *cp = code;
	int i = 0;
	for (i = 0; i < READ_BITS - 8; i++)
	{
		if (!consist_synchro && (*cp < 128 && *(cp + 1) > 128 && *(cp + 2) < 128 && *(cp + 3) > 128))
		{
			syn_found = true;
			break;
		}

		if (consist_synchro && (*cp < 127 && *(cp + 1) > 129 && *(cp + 2) < 127 && *(cp + 3) > 129))
		{
			syn_found = true;
			break;
		}

		cp++;
	}
	if (!syn_found)
		return 0xFF;
	//Nous avons trouvé la séquence, mais nous devons nous assurer qu’il existe avant un perembula, c’est-à-dire des unités confiantes ou un signal illisible.
	// Le préambule doit être examiné à 16-3 = 13 bits en arrière. Eh bien, au moins 10 !!!

	for (byte g = i; i - g < 10 && g > 0; g--)
	{
		cp--;
		if (*cp < 127)
			return 0xFF; // Avant la synchronisation dans le préambule, il ne peut y avoir de zéro sûr.Pas de synchronisation ici
	}
	return (byte)i;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Crée une parcelle de code
// code - un pointeur sur la séquence de bits déchiffrée
// résultat - un pointeur sur la parcelle de code
// valide - un pointeur sur la carte de parité du code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_info_data(byte *code, byte *result, byte *valid)
{

	byte *rd = result;
	byte *vd = valid;
	//Nous nettoyons les tableaux
	for (int l = 0; l < PACKET_LENGTH; l++)
	{
		*vd = 0;
		*rd = 0;
		vd++;
		rd++;
	}
	rd = result;
	vd = valid;

	int csm;
	for (csm = 0; csm < 30; csm++)
	{
		if (!consist_synchro && (*code < 128 && *(code + 1) > 128 && *(code + 2) < 128 && *(code + 3) > 128))
			break; //Найдена последовательность 0101
		if (consist_synchro && (*code < 127 && *(code + 1) > 129 && *(code + 2) < 127 && *(code + 3) > 129))
			break;
		code++;
	}
	// 	La séquence de démarrage dans les 20 premiers bits n’a pas été trouvée, un tel paquet ne peut pas être décodé par cette méthode.

	if (csm > 22)
		return 0;
	//Aller au début de la lecture
	code += 4;
	int ii = 0;
	for (int i = 0; i < READ_BITS - csm; i++)
	{
		// Чтобы не выйти за пределы
		if (i >= PACKET_LENGTH * 4)
			break;
		byte multipl = 0;
		switch (ii)
		{
		case 0:
		{
			multipl = 0x01;
			break;
		}
		case 1:
		{
			multipl = 0x02;
			break;
		}
		case 2:
		{
			multipl = 0x04;
			break;
		}
		case 3:
		{
			multipl = 0x08;
			break;
		}
		}
		if (*code == 129)
			*rd += multipl;
		if (*code > 129)
		{
			*rd += multipl;
			*vd += multipl;
		}
		if (*code < 127)
			*vd += multipl;
		code++;
		ii++;
		if (ii == 4)
		{
			ii = 0;
			vd++;
			rd++;
		}
	}
	return 1;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Renvoie la valeur de la température
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
float Oregon_NR::get_temperature(byte *oregon_data)
{
	float tmprt = 0;
	oregon_data += 8;
	//corriger les erreurs possibles:
	for (int g = 0; g < 4; g++)
		if (*(oregon_data + g) > 9)
			*(oregon_data + g) = *(oregon_data + g) - 8;
	tmprt += *(oregon_data)*0.1;
	tmprt += *(oregon_data + 1);
	tmprt += *(oregon_data + 2) * 10;
	return (*(oregon_data + 3)) ? -tmprt : tmprt;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Retourne le type de capteur
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
word Oregon_NR::get_sensor(byte *oregon_data)
{
	return (word)(*(oregon_data)) * 0x1000 + (*(oregon_data + 1)) * 0x0100 + (*(oregon_data + 2)) * 0x10 + *(oregon_data + 3);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Retourne la valeur du canal.
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_channel(byte *oregon_data)
{

	byte channel = 0;
	switch (*(oregon_data + 4))
	{
	case 1:
		channel = 1;
		break;
	case 2:
		channel = 2;
		break;
	case 4:
		channel = 3;
		break;
	}
	return channel;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_battery(byte *oregon_data)
{

	return (*(oregon_data + 7) & 0x4) ? 0 : 1;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Renvoie la valeur d'humidité
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_humidity(byte *oregon_data)
{

	byte tmprt;
	oregon_data += 12;
	//corrigez les erreurs possibles:
	for (int g = 0; g < 2; g++)
		if (*(oregon_data + g) > 9)
			*(oregon_data + g) = *(oregon_data + g) - 8;
	tmprt = *(oregon_data);
	tmprt += *(oregon_data + 1) * 10;
	return tmprt;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Renvoie l'identifiant du capteur
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_id(byte *oregon_data)
{

	byte tmprt;
	oregon_data += 5;
	tmprt = *(oregon_data)*0x10;
	tmprt += *(oregon_data + 1);
	return tmprt;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//Contrôle CRC
// oregon_data - un pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
bool Oregon_NR::check_CRC(byte *oregon_data, word sensor_type)
{

	byte *pp = oregon_data;
	byte crc, resived_crc, truecrc, resived_truecrc, i;
	crc = 0;
	byte CCIT_POLY = 0x07;

	if (sensor_type == THGN132)
	{
		truecrc = 0x3C;
		for (int x = 0; x < 15; x++)
		{
			crc += *pp;
			if (x != 5 && x != 6)
			{
				truecrc ^= *pp;
				for (i = 0; i < 4; i++)
					if (truecrc & 0x80)
						truecrc = (truecrc << 1) ^ CCIT_POLY;
					else
						truecrc <<= 1;
				//truecrc &= 0xff;
			}
			pp++;
		}
		for (i = 0; i < 4; i++)
			if (truecrc & 0x80)
				truecrc = (truecrc << 1) ^ CCIT_POLY;
			else
				truecrc <<= 1;

		resived_crc = (*(oregon_data + 15)) + (*(oregon_data + 16)) * 0x10;
		resived_truecrc = (*(oregon_data + 17)) + (*(oregon_data + 18)) * 0x10;
		received_CRC = truecrc;
		return (resived_crc == crc && resived_truecrc == truecrc) ? 1 : 0;
	}

	if ((sensor_type & 0xFF00) == GAS || (sensor_type & 0xFF00) == FIRE)
	{
		truecrc = 0x00;
		for (int x = 0; x < 15; x++)
		{
			crc += *pp;
			truecrc ^= *pp;
			for (i = 0; i < 4; i++)
				if (truecrc & 0x80)
					truecrc = (truecrc << 1) ^ CCIT_POLY;
				else
					truecrc <<= 1;
			//truecrc &= 0xff;
			pp++;
		}
		for (i = 0; i < 4; i++)
			if (truecrc & 0x80)
				truecrc = (truecrc << 1) ^ CCIT_POLY;
			else
				truecrc <<= 1;

		resived_crc = (*(oregon_data + 15)) + (*(oregon_data + 16)) * 0x10;
		resived_truecrc = (*(oregon_data + 17)) + (*(oregon_data + 18)) * 0x10;
		received_CRC = truecrc;
		return (resived_crc == crc && resived_truecrc == truecrc) ? 1 : 0;
		//return (resived_crc == crc)? 1 : 0;
	}

	if (sensor_type == THN132)
	{
		truecrc = 0xD6;
		for (int x = 0; x < 12; x++)
		{
			crc += *pp;
			if (x != 5 && x != 6)
			{
				truecrc ^= *pp;
				for (i = 0; i < 4; i++)
					if (truecrc & 0x80)
						truecrc = (truecrc << 1) ^ CCIT_POLY;
					else
						truecrc <<= 1;
				//truecrc &= 0xff;
			}
			pp++;
		}
		for (i = 0; i < 4; i++)
			if (truecrc & 0x80)
				truecrc = (truecrc << 1) ^ CCIT_POLY;
			else
				truecrc <<= 1;

		resived_crc = (*(oregon_data + 12)) + (*(oregon_data + 13)) * 0x10;
		resived_truecrc = (*(oregon_data + 14)) + (*(oregon_data + 15)) * 0x10;
		received_CRC = truecrc;
		return (resived_crc == crc && resived_truecrc == truecrc) ? 1 : 0;
	}
	return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Récupération de données par type de capteur
////////////////////////////////////////////////////////////////////////////////////////////////////
void Oregon_NR::restore_data(byte *oregon_data, word sensor_type)
{

	byte *pp = oregon_data;
	if (sensor_type == THGN132)
	{
		pp += 8;
		for (int x = 0; x < 6; x++)
		{
			if (*pp > 9 && x != 3)
				*pp -= 8;
			pp++;
		}
	}
	if (sensor_type == THN132)
	{
		pp += 8;
		for (int x = 0; x < 3; x++)
		{
			if (*pp > 9)
				*pp -= 8;
			pp++;
		}
	}
	return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//Fonctions de décryptage des données du capteur GAS
// gas_data - pointeur sur la parcelle de code
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_gas_channel(byte *gas_data)
{

	return gas_data[2];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
float Oregon_NR::get_gas_temperature_out(byte *gas_data)
{

	int temperat = gas_data[9] * 0x0100 + gas_data[8] * 0x0010 + gas_data[7];
	return ((float)(-1000 + temperat)) / 10;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
float Oregon_NR::get_gas_temperature_in(byte *gas_data)
{

	int temperat = gas_data[12] * 0x0100 + gas_data[11] * 0x0010 + gas_data[10];
	return ((float)(-1000 + temperat)) / 10;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_gas_hmdty(byte *gas_data)
{

	return gas_data[14] * 0x10 + gas_data[13];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_gas_CO(byte *gas_data)
{

	return gas_data[6] * 0x10 + gas_data[5];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_gas_CH(byte *gas_data)
{

	return gas_data[4] * 0x10 + gas_data[3];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void Oregon_NR::led_light(bool led_on)
{
	if (LED != 0xFF)
	{
		if (PULL_UP && led_on)
			digitalWrite(LED, LOW);
		if (PULL_UP && !led_on)
			digitalWrite(LED, HIGH);
		if (!PULL_UP && led_on)
			digitalWrite(LED, HIGH);
		if (!PULL_UP && !led_on)
			digitalWrite(LED, LOW);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_fire_ip22(byte *fire_data)
{

	return fire_data[4] * 0x10 + fire_data[5];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_fire_ip72(byte *fire_data)
{

	return fire_data[6] * 0x10 + fire_data[7];
}
////////////////////////////////////////////////////////////////////////////////////////////////////
byte Oregon_NR::get_fire_lockalarm(byte *fire_data)
{

	return fire_data[8] * 0x10 + fire_data[9];
}