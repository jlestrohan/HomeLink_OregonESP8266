/**
 * IotWebConf01Minimal.ino -- IotWebConf is an ESP8266/ESP32
 *   non blocking WiFi/AP web configuration library for Arduino.
 *   https://github.com/prampec/IotWebConf 
 *
 * Copyright (C) 2020 Balazs Kelemen <prampec+arduino@gmail.com>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

/**
 * Example: Minimal
 * Description:
 *   This example will shows the bare minimum required for IotWebConf to start up.
 *   After starting up the thing, please search for WiFi access points e.g. with
 *   your phone. Use password provided in the code!
 *   After connecting to the access point the root page will automatically appears.
 *   We call this "captive portal".
 *   
 *   Please set a new password for the Thing (for the access point) as well as
 *   the SSID and password of your local WiFi. You cannot move on without these steps.
 *   
 *   You have to leave the access point before to let the Thing continue operation
 *   with connecting to configured WiFi.
 *
 *   Note that you can find detailed debug information in the serial console depending
 *   on the settings IOTWEBCONF_DEBUG_TO_SERIAL, IOTWEBCONF_DEBUG_PWD_TO_SERIAL set
 *   in the IotWebConf.h .
 */

#include "Oregon_NR.h"
#include <NTPClient.h>
#include <MQTT.h>
#include <IotWebConf.h>
#include <IotWebConfUsing.h> // This loads aliases for easier class names.
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson
#include "RemoteDebug.h"        //https://github.com/JoaoLopesF/RemoteDebug
#include <ArduinoOTA.h>
#include "uptime_formatter.h"
#include <string.h>

// function headers
void setupOTA();
void setup();
void loop();
void handleRoot();
void wifiConnected();
void configSaved();
void oregonCapture();
void mqttMessageReceived(String &topic, String &payload);
bool formValidator(iotwebconf::WebRequestWrapper* webRequestWrapper);

#define NTP_OFFSET 60 * 60     // In seconds
#define NTP_INTERVAL 60 * 1000 // In miliseconds
#define NTP_ADDRESS "europe.pool.ntp.org"
#define THINGNAME "Oregon_Homelink"
#define STRING_LEN 64
#define CONFIG_VERSION "v1.2.1"
#define WIFI_INITIAL_PW "73727170" // Initial password to connect to the Thing, when it creates an own Access Point.
#define CONFIG_PIN 5  /* -- When CONFIG_PIN is pulled to ground on startup, the Thing will use the initial */
                      /*      password to buld an AP. (E.g. in case of lost password) */
#define MQTT_LED 4
#define STATUS_PIN LED_BUILTIN

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

RemoteDebug Debug;

// -- Method declarations.
void handleRoot();
void mqttMessageReceived(String &topic, String &payload);
bool connectMqtt();
bool connectMqttOptions();
// -- Callback methods.
void wifiConnected();
void configSaved();
bool formValidator(iotwebconf::WebRequestWrapper* webRequestWrapper);
void oregonCapture();
bool connectMqtt();
bool connectMqttOptions();

DNSServer dnsServer;
WebServer server(80);
WiFiClient net;
MQTTClient mqttClient;

char mqttServerValue[STRING_LEN];
char mqttUserNameValue[STRING_LEN];
char mqttUserPasswordValue[STRING_LEN];
char mqttPublishTopicValue[STRING_LEN];

// for Oregon -----------------------------------------------------------------------------------------------------------------
const byte interruptPin = D7; // mapped to GPIO13 https://github.com/esp8266/Arduino/issues/584
// Oregon sensor setup
Oregon_NR oregon(interruptPin, true);

IotWebConf iotWebConf(THINGNAME, &dnsServer, &server, WIFI_INITIAL_PW);
// -- You can also use namespace formats e.g.: iotwebconf::ParameterGroup
IotWebConfParameterGroup mqttGroup = IotWebConfParameterGroup("mqtt", "MQTT configuration");
IotWebConfTextParameter mqttServerParam = IotWebConfTextParameter("MQTT server", "mqttServer", mqttServerValue, STRING_LEN);
IotWebConfTextParameter mqttUserNameParam = IotWebConfTextParameter("MQTT user", "mqttUser", mqttUserNameValue, STRING_LEN);
IotWebConfPasswordParameter mqttUserPasswordParam = IotWebConfPasswordParameter("MQTT password", "mqttPass", mqttUserPasswordValue, STRING_LEN);
IotWebConfTextParameter mqttPublishTopicParam = IotWebConfTextParameter("MQTT Topic", "mqttTopic", mqttPublishTopicValue, STRING_LEN, "text", "jles/RFLink");

bool needMqttConnect = false;
bool needReset = false;
int pinState = HIGH;
unsigned long lastReport = 0;
unsigned long lastMqttConnectionAttempt = 0;

/**
 * @brief  
 * @note   
 * @retval None
 */
void setupOTA()
{
  ArduinoOTA.onStart([]() {
    char ota_type[15];
    if (ArduinoOTA.getCommand() == U_FLASH)
    {
      strcpy(ota_type, "sketch");
    }
    else
    { // U_FS
      strcpy(ota_type, "filesystem");
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    debugD("Start updating %s", ota_type);
  });
  ArduinoOTA.onEnd([]() {
    debugD("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    debugD("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    debugE("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      debugE("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      debugE("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      debugE("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      debugE("Receive Failed");
    }else if (error == OTA_END_ERROR) {
      debugE("End Failed");
    }
  });
  ArduinoOTA.begin();
}


void setup() 
{
  Debug.begin(THINGNAME);
  Serial.begin(115200);
  debugI("Starting up...\n");
  pinMode(MQTT_LED, OUTPUT);
  timeClient.begin();

  ArduinoOTA.setHostname(THINGNAME); // on donne une petit nom a notre module   
  // ArduinoOTA.setPassword("12345678c");   
  ArduinoOTA.begin(); // initialisation de l'OTA

  mqttGroup.addItem(&mqttServerParam);
  mqttGroup.addItem(&mqttUserNameParam);
  mqttGroup.addItem(&mqttUserPasswordParam);
  mqttGroup.addItem(&mqttPublishTopicParam);

  // -- Initializing the configuration.
  iotWebConf.setStatusPin(STATUS_PIN);
  iotWebConf.setConfigPin(CONFIG_PIN);
  iotWebConf.addParameterGroup(&mqttGroup);
  iotWebConf.setConfigSavedCallback(&configSaved);
  iotWebConf.setFormValidator(&formValidator);
  iotWebConf.setWifiConnectionCallback(&wifiConnected);
    
  bool validConfig = iotWebConf.init();
  if (!validConfig)
  {
    mqttServerValue[0] = '\0';
    mqttUserNameValue[0] = '\0';
    mqttUserPasswordValue[0] = '\0';
    mqttPublishTopicValue[0] = '\0';
  }

  // -- Set up required URL handlers on the web server.
  server.on("/", handleRoot);
  server.on("/config", []{ iotWebConf.handleConfig(); });
  server.onNotFound([](){ iotWebConf.handleNotFound(); });

  mqttClient.begin(mqttServerValue, net);
  mqttClient.onMessage(mqttMessageReceived);

  debugD("Starting Oregon sensor interruption");
  oregon.start();

  setupOTA();

  debugI("Setup completed succesfully!...");
}

void loop() 
{
  // -- doLoop should be called as frequently as possible.
  iotWebConf.doLoop();
  mqttClient.loop();
  timeClient.update();
  ArduinoOTA.handle();
  Debug.handle();
  
  oregonCapture();

  if (needMqttConnect)
  {
    if (connectMqtt())
    {
      needMqttConnect = false;
    }
  }
  else if ((iotWebConf.getState() == iotwebconf::OnLine) && (!mqttClient .connected()))
  {
    debugI("MQTT reconnecting...");
    connectMqtt();
  }

  if (needReset)
  {
    debugI("Rebooting after 1 second.");
    iotWebConf.delay(1000);
    ESP.restart();
  }

  /*unsigned long now = millis();
  if ((500 < now - lastReport) && (pinState != digitalRead(CONFIG_PIN)))
  {
    pinState = 1 - pinState; // invert pin state as it is changed
    lastReport = now;
    Serial.print("Sending on MQTT channel '/test/status' :");
    Serial.println(pinState == LOW ? "ON" : "OFF");
    //mqttClient.publish("/test/status", pinState == LOW ? "ON" : "OFF");
  }*/
}

/**
 * Handle web requests to "/" path.
 */
void handleRoot()
{
  // -- Let IotWebConf test and handle captive portal requests.
  if (iotWebConf.handleCaptivePortal())
  {
    // -- Captive portal request were already served.
    return;
  }
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
  s += "<title>IotWebConf 01 Minimal</title></head><body>";
  s += "Go to <a href='config'>configure page</a> to change settings.";
  s += "</body></html>\n";

  server.send(200, "text/html", s);
}

void wifiConnected()
{
  debugV("Wifi connected...");
  needMqttConnect = true;
}

void configSaved()
{
  debugI("Configuration was updated.");
  needReset = true;
}

/**
 * 
 * 
 */
bool formValidator(iotwebconf::WebRequestWrapper* webRequestWrapper)
{
  debugV("Validating form.");
  bool valid = true;

  int l = webRequestWrapper->arg(mqttServerParam.getId()).length();
  if (l < 3)
  {
    mqttServerParam.errorMessage = "Please provide at least 3 characters!";
    valid = false;
  }

  return valid;
}

/**
 * @brief   Oregon Packets capture routine
 * @note   
 * @retval  None
 */
void oregonCapture()
{
  //Packet capture
  oregon.capture(false); //(true = debug info)
  //	Les données capturées sont valables jusqu'au prochain appel capturé
  if (oregon.captured)
  {
    debugV("Oregon packet detected... processing");

    digitalWrite(MQTT_LED, HIGH);

    StaticJsonDocument<300> JSONbuffer;
    JsonObject JSONencoder = JSONbuffer.createNestedObject();

    if ((oregon.sens_type == THGN132 || oregon.sens_type == THN132) && oregon.crc_c)
    {

      if (oregon.sens_type == THGN132)
        JSONencoder["sensorType"] = "THGN132N";
      if (oregon.sens_type == THN132)
        JSONencoder["sensorType"] = "THN132N";

      JSONencoder["channel"] = oregon.sens_chnl;
      JSONencoder["id"] = oregon.sens_id;

      JSONencoder["temperature"] = oregon.sens_tmp;
      JSONencoder["unit"] = "C";

      if (oregon.sens_type == THGN132)
        JSONencoder["hum_percent"] = oregon.sens_hmdty;

      if (oregon.sens_battery)
        JSONencoder["battery"] = "F";
      else
        JSONencoder["battery"] = "E";

      JSONencoder["timestamp"] = timeClient.getEpochTime();

      char JSONmessageBuffer[150];
      serializeJson(JSONencoder, JSONmessageBuffer);

      debugI("%s",JSONmessageBuffer);
      debugV("uptime: %s", uptime_formatter::getUptime().c_str());

      // publish to MQTT
      if (mqttClient.publish(mqttPublishTopicValue, JSONmessageBuffer)) {
          debugV("Success sending MQTT");
      } else {
          debugE("Error sending MQTT (error: %d", mqttClient.returnCode());
      }
    }
    digitalWrite(MQTT_LED, LOW);
  }
}

/**
 * 
 * 
 */
bool connectMqtt() {
  unsigned long now = millis();
  if (1000 > now - lastMqttConnectionAttempt)
  {
    // Do not repeat within 1 sec.
    return false;
  }
  debugV("Connecting to MQTT server...");
  if (!connectMqttOptions()) {
    lastMqttConnectionAttempt = now;
    return false;
  }
  debugV("MQTT Connected!");

  mqttClient.subscribe("/jles/RFLink");
  return true;
}

/*
// -- This is an alternative MQTT connection method.
bool connectMqtt() {
  Serial.println("Connecting to MQTT server...");
  while (!connectMqttOptions()) {
    iotWebConf.delay(1000);
  }
  Serial.println("Connected!");
  mqttClient.subscribe("/test/action");
  return true;
}
*/

/**
 * 
 * 
 */
bool connectMqttOptions()
{
  bool result;
  if (mqttUserPasswordValue[0] != '\0')
  {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue, mqttUserPasswordValue);
  }
  else if (mqttUserNameValue[0] != '\0')
  {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue);
  }
  else
  {
    result = mqttClient.connect(iotWebConf.getThingName());
  }
  return result;
}

/**
 * 
 * 
 */
void mqttMessageReceived(String &topic, String &payload)
{
  debugV("Incoming: %s - %s", topic.c_str(), payload.c_str());
}
