# HomeLink_OregonESP8266
Retrieves Oregon temperature probes signal and send a formatted JSON to a MQTT broker. 

Aimed to ESP8266-12F devices

Connect a RF433 module data pin to pin 12 of the 8266 module. The radio module must be powered with 5V, the ESP8266 3.3V

The application listens to RF433 packets, and decodes Oregon probes frames.

You can check the logs via telnet by connecting to the device IP on port 23. Firmware updates can be made via OTA.


Based on original work by Sergey Zawislak
https://github.com/invandy/Oregon_NR
